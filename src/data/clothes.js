// images
import cloth01 from '../assets/clothes/cloth01.jpg';
import cloth02 from '../assets/clothes/cloth02.jpg';
import cloth03 from '../assets/clothes/cloth03.jpg';
import cloth04 from '../assets/clothes/cloth04.jpg';
import cloth05 from '../assets/clothes/cloth05.jpg';
import cloth06 from '../assets/clothes/cloth06.jpg';
import cloth07 from '../assets/clothes/cloth07.jpg';
import cloth08 from '../assets/clothes/cloth08.jpg';
import cloth09 from '../assets/clothes/cloth09.jpg';

// data
export const clothes = [
	{
		id: 1,
		name: 'Hacci Pullover',
		price: 68,
		image: cloth01,
	},
	{
		id: 2,
		name: 'Blouson Crop',
		price: 32,
		image: cloth02,
	},
	{
		id: 3,
		name: 'Sunny days Bodysuit',
		price: 58,
		image: cloth03,
	},
	{
		id: 4,
		name: 'Strapeze slip',
		price: 69,
		image: cloth04,
	},
	{
		id: 5,
		name: 'Angelica Kimono',
		price: 88,
		image: cloth05,
	},
	{
		id: 6,
		name: 'Elodie Festival Trousers',
		price: 58,
		image: cloth06,
	},
	{
		id: 7,
		name: 'All mine Tee',
		price: 38,
		image: cloth07,
	},
	{
		id: 8,
		name: 'Sugar Sands Jumpsuit',
		price: 79,
		image: cloth08,
	},
	{
		id: 9,
		name: 'Take the plunge Tank',
		price: 32,
		image: cloth09,
	},
];