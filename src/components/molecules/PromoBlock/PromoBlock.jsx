import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './PromoBlock.css';

const PromoBlock = ({ image, title, subtitle }) => (
  <div className="promo-block" style={{backgroundImage: `url(${image})`}}>
    <p>{subtitle}</p>
    <div className="overlay">
      <h3>Shop {title}</h3>
    </div>
 </div>
);

PromoBlock.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

PromoBlock.defaultProps = {
  image: '',
  title: '',
  subtitle: '',
};

export default PromoBlock;
