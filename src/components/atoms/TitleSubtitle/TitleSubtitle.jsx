import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './TitleSubtitle.css';

const TitleSubtitle = ({ title, subtitle }) => (
  <div className="title-subtitle">
    <h2>{subtitle}</h2>
    <h1>{title}</h1>
 </div>
);

TitleSubtitle.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

TitleSubtitle.defaultProps = {
  title: '',
  subtitle: '',
};

export default TitleSubtitle;
