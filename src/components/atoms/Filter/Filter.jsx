import React from 'react';
// Styles
import './Filter.css';

const Filter = () => (
  <div className="filter offset-1">
    <div className="sort-by">
      <span>Sort by:</span> Most Popular
    </div>
    <div className="filter-by">
      <span>Filter by:</span> Trend
    </div>
 </div>
);

export default Filter;
