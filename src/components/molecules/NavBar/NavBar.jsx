import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { NavLink } from 'react-router-dom';
// Actions
import { fetchUser } from "../../../redux/user";
// Images
import logo from '../../../assets/logo.svg';
// Styles
import './NavBar.css';

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = { navOpen: false };
    this.toggleNav = this.toggleNav.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchUser(2));
  }

  toggleNav() {
    const { navOpen } = this.state;

    this.setState({ navOpen: !navOpen });
  }

  render() {
    const {
      data,
      error,
      loading,
    } = this.props;
    const { navOpen } = this.state;

    return (
      <div className="navbar">
        <div className="top-bar col-14 offset-1-xs">
          <div className="cart no-items">
            <i className="fas fa-shopping-basket" />
          </div>
          <div className="logo">
            <NavLink to="/">
              <img src={logo} alt="Indigo Apparel logo" />
            </NavLink>
          </div>
          <div className="user">
            
            {!error && !loading && (<React.Fragment><div className="image" style={{backgroundImage: `url(${data.avatar})`}} />
            <p>Hi {data.first_name}!</p>
            </React.Fragment>)}
          </div>
          <div className="menu-toggle">
            <i className="fas fa-bars" onClick={this.toggleNav} />
          </div>
        </div>
        <div className={`menu ${navOpen ? 'active' : ''}`} onClick={this.toggleNav}>
          <ul>
            <li>
              <NavLink to="/clothes" activeClassName="active">
                Clothes
              </NavLink>
            </li>
            <li>
              Shoes
            </li>
            <li>
              Accessories
            </li>
            <li>
              Beauty
            </li>
            <li>
              Lifestyle
            </li>
          </ul>
        </div>
     </div>
    );
  }
}

NavBar.propTypes = {
  data: PropTypes.any,
  loading: PropTypes.bool,
  error: PropTypes.any,
};

NavBar.defaultProps = {
  data: {},
  loading: false,
  error: '',
};

const mapStateToProps = state => ({
  data: state.user.data,
  loading: state.user.loading,
  error: state.user.error,
});

export default connect(mapStateToProps)(NavBar);
