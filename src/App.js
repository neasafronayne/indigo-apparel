import React, { Component } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
// Components
import NavBar from './components/molecules/NavBar/NavBar';
import Home from './pages/Home/Home';
import Clothes from './pages/Clothes/Clothes';
//Global css import
import './style/global.css';

class App extends Component {
  render() {
    return (
      <div className="App">
      	<NavBar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/clothes" exact component={Clothes} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
