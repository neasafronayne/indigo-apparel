import React from 'react';
// Styles
import './SocialPanel.css';

const SocialPanel = () => (
  <div className="social-panel">
    <div className="sticky">
      <div className="social">
        <i className="fab fa-instagram" />
      </div>
      <div className="social">
        <i className="fab fa-facebook-f" />
      </div>
      <div className="social">
        <i className="fab fa-twitter" />
      </div>
   </div>
 </div>
);

export default SocialPanel;
